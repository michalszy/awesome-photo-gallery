@extends('master')
@section('content')
    <div class="container spark-screen">
        <div class="row">
            <div class="col-md-12">
                        <h1 class="page-header">Administration panel
                            <small>Upload new images</small>
                        </h1>
                @if($errors->has())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <p> {{ $error }}</p>
                        @endforeach
                    </div>
                @else
                    <div class="alert alert-info">
                        <p>Only images type files.</p>
                        <p>Maximum size 2mb.</p>
                    </div>
                    @endif
                        {!! Form::open(array('url'=>'panel/add','method'=>'POST', 'files'=>true)) !!}
                        <div class="control-group">
                            <div class="controls">
                                {!! Form::file('images[]', array('multiple'=>true)) !!}
                                <p class="errors">{!!$errors->first('images')!!}</p>
                                @if(Session::has('error'))
                                    <p class="errors">{!! Session::get('error') !!}</p>
                                @endif
                            </div>
                        </div>
                        {{ Form::hidden('album_id', $album_id, array('id' => 'album_id')) }}
                        {!! Form::submit('Submit', array('class'=>'send-btn')) !!}
                        {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
