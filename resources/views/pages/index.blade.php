@extends('master')
@section('content')
    <div class="container spark-screen">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {!! Session::get('success') !!}.
                    </div>
                @endif
                @if(count($albums) > 0)
                @foreach($albums as $album)
                    <div class="row">
                        <div class="col-md-4 col-md-offset-3">
                            <a href={{ url('/album') }}/{{ $album->id }}">
                                @if(count($album->photos) > 0)
                                <img class="img-responsive" src="{{ URL::asset('uploads/thumbs') }}/{{ $album->photos->first()->image }}">
                                    @else
                                    <img class="img-responsive" src="{{ URL::asset('photo.jpg') }}" alt="">
                                    @endif
                            </a>
                        </div>
                        <div class="col-md-5">
                            <h3>{{ str_limit($album->title, 15) }}</h3>
                            <h4>{{ $album->published_at }}</h4>
                            <p>{{ $album->description }}</p>
                            <a class="btn btn-primary" href="{{ url('/album') }}/{{ $album->id }}">Open album <span class="glyphicon glyphicon-chevron-right"></span></a>
                        </div>
                    </div>


                    <hr>
                    @endforeach
            </div>

                    @else
                        <div class="col-md-12 text-center">
                            <h1>Sorry there is no any album.<small> But you always can create new one.</small></h1>
                        </div>
                    @endif

            </div>
            <div class="col-md-12 text-center">
                {!! $albums->render() !!}
            </div>
        </div>
    </div>
@endsection
