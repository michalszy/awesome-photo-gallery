@extends('master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-header">{{ $album->title }}
                    <small>{{ $album->published_at }}</small>
                </h1>
            </div>
        </div>
        <div class="row">
            @foreach($photo as $image)
                <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                    <a class="test-popup-link" href="#">
                        <a href="{{ URL::asset('uploads/images') }}/{{ $image->image }}" data-lightbox="image"><img class="img-responsive" src="{{ URL::asset('uploads/thumbs') }}/{{ $image->image }}" alt="{{ $image->id }}"></a>

                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection
@section('scripts')