@extends('master')
@section('content')
    <div class="container spark-screen">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-header">Administration panel
                    <small>Create new album</small>
                </h1>
                @if($errors->has())
                    <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                            <p> {{ $error }}</p>
                    @endforeach
                    </div>
                    @else
                    <div class="alert alert-info">
                        <p><b>Title</b> - only alphanumeric characters.</p>
                        <p><b>Descriptions</b> - maximum 150 characters.</p>
                    </div>
                @endif
                {!! Form::open(array('url' => '/panel/create', 'method' => 'POST')) !!}
                <div class="form-group">
                    {!! Form::label('title', 'Title: ') !!}

                    {!! Form::text('title', null, ['class' => 'form-controls']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('description', 'Description: ') !!}

                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                </div>{!! Form::submit('Create Album', ['class' => 'btn btn-primary form-control']) !!}
                {!! Form::close() !!}
            </div>
            <div class="col-md-12 text-center">

            </div>
        </div>
    </div>
@endsection
