@extends('master')
@section('content')
<div class="container spark-screen">
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-header">Administration panel
                <small>Manage your image galleries</small>
            </h1>
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <strong>Success!</strong> {!! Session::get('success') !!}.
                </div>
            @endif
            <table class="table">
                <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Created by</th>
                            <th>Number of photos</th>
                            <th>Created at</th>
                            <th>Add Images</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                <tbody>
                    @foreach($albums as $Album)
                        <tr>
                            <th scope="row">{{ $index ++}}</th>
                            <td>{{ str_limit($Album->title, 15) }}</td>
                            <td>{{ $Album->user->name }}</td>
                            <td>{{ $Album->photos->count() }}</td>
                            <td>{{ $Album->published_at }}</td>
                            <td><a href="{{ url('/panel/add') }}/{{ $Album->id }}"><i class="fa fa-plus fa-lg"></i></a> </td>
                            <td><a href="{{ url('/panel/delete') }}/{{ $Album->id }}"><i class="fa fa-trash fa-lg"></i></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-12 text-center">
            {!! $albums->render() !!}
        </div>
    </div>
</div>
@endsection
