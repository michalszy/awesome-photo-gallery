<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
    Route::get('/', 'AlbumsController@index');

    Route::get('/album/{id}', 'AlbumsController@album');

    Route::get('/panel', 'PanelController@index');

    Route::get('/panel/delete/{id}', 'PanelController@delete');

    Route::get('/panel/create', 'PanelController@createAlbum');

    Route::post('/panel/create', 'PanelController@createAlbumPost');

    Route::get('/panel/add/{id}', 'PanelController@addImages');

    Route::post('/panel/add', 'PanelController@addImagesPost');





});


Route::group(['middleware' => 'web'], function () {
    Route::auth();
});
