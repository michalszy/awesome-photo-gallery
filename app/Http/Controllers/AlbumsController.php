<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Album;
use App\Photo;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Traits\MacroableTrait;


class AlbumsController extends Controller
{
    //
    public function index(){
        $albums = Album::with('Photos')->paginate(5);
        return view('pages.index')->with('albums', $albums);
    }

    public function album($id){
        $album = Album::with('Photos')->find($id);
        $photo = Photo::where('album_id', '=', $id)->paginate(12);
        return view('pages.album')->with('album', $album)->with('photo', $photo);
    }
}
