<?php

namespace App\Http\Controllers;

use App\Album;
use App\Photo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class PanelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $index = 1;
        $albums = Album::with('User')->with('Photos')->paginate(10);
        return view('pages.panel')->with('albums', $albums)->with('index', $index);
    }

    public function delete($id)
    {
        Album::where('id', '=', $id)->delete();
        return back();
    }

    public function createAlbum()
    {
        return view('pages.create');
    }

    public function createAlbumPost()
    {
        $input = Request::all();
        $validation = Validator::make($input, ['title' => 'required|min:3|max:255|alpha_num', 'description' =>'required']);
        if($validation->fails())
        {
            return redirect('panel/create')
                ->withErrors($validation)
                ->withInput();
        }
        else
        {
            $album = new Album();
            $album->title = $input['title'];
            $album->description = $input['description'];
            $album->user_id =  Auth::user()->id;
            $album->save();
            Session::flash('success', 'New album '.$input['title'].' has been created! Now you can add new images.');
            return Redirect::to('panel');
        }
    }

    public function addImages($id)
    {
        return view('pages.add')->with('album_id', $id);
    }

    public function addImagesPost()
    {
        $files = Input::file('images');
        $file_count = count($files);
        $uploadcount = 0;
        foreach($files as $file)
        {
            $rules = array('file' => 'required|image');
            $validator = Validator::make(array('file' => $file), $rules);
            if($validator->fails())
            {

                return redirect('panel/add/'.Request::input('album_id'))
                    ->withErrors($validator)
                    ->withInput();
            } else{
                $time = Carbon::now()->format('YmdHis');
                $destinationPath = 'uploads/images'; // upload path
                $extension = $file->getClientOriginalExtension();
                $fileName = $time.'.'.str_random(30).'.'.$extension;
                $file->move($destinationPath, $fileName);
                $filePath = $destinationPath.'/'.$fileName;
                $thumbPath = 'uploads/thumbs/'.$fileName;
                $thumb = Image::make($filePath);
                $thumb->resize(400, 300);
                $thumb->save($thumbPath);
                $photo = new Photo();
                $photo->user_id = Auth::user()->id;
                $photo->image = $fileName;
                $photo->album_id = Request::input('album_id');
                $photo->save();
                $uploadcount ++;
            }
        }

        if($uploadcount == $file_count)
        {
            Session::flash('success', 'Upload successfully');
            return Redirect::to('panel');
        }
    }
}
